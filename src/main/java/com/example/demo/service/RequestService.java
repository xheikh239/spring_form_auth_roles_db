package com.example.demo.service;


import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.RequestForm;
import com.example.demo.repository.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.temporal.ChronoUnit;
import java.util.List;


@Service(value = "requestService")
@Transactional
public class RequestService {
    private final RequestRepository requestRepository;

    @Autowired
    public RequestService(RequestRepository requestRepository){
        this.requestRepository = requestRepository;
    }

    public RequestForm applyRequest(RequestForm requestForm) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String CurrentUser = authentication.getName();
        requestForm.setMatricule(CurrentUser);
        long period = ChronoUnit.DAYS.between(requestForm.getDateDebut(), requestForm.getDateFin());
        requestForm.setDuration(period);
        requestForm.setActive(true);
      return requestRepository.save(requestForm);
    }

    public List<RequestForm> findAllRequests() {
        return requestRepository.findAll();
    }

    public RequestForm updateRequest(RequestForm request) {
        return requestRepository.save(request);
    }

    public void deleteRequestById(long id) {
        requestRepository.deleteRequestById(id);
    }


    public RequestForm findRequestById(Long id){
        return requestRepository.findRequestById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Request by id " + id + "was not found"));
    }


    public List<RequestForm> getAllActiveRequests() {
        return requestRepository.getAllActiveRequests();
    }

    public List<RequestForm> getAllRequestsOfUser(String matricule) {
        return requestRepository.getAllRequestsOfUser(matricule);
    }

/*    public RequestForm fRequestById(Long id) {
        return requestRepository.findByidsql(id);
    }*/
}
