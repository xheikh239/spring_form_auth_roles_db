package com.example.demo.repository;

import com.example.demo.model.RequestForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;


@Repository(value = "requestRepository")
public interface RequestRepository extends JpaRepository<RequestForm, Serializable>{

    Optional<RequestForm> findRequestById(Long id);

    void deleteRequestById(long id);

    @Query(nativeQuery = true, value = "select * from demande_conge where matricule=? order by id desc")
List<RequestForm> getAllRequestsOfUser(String matricule);

    @Query(nativeQuery = true, value = "select * from demande_conge where active=true")
List<RequestForm> getAllActiveRequests();

/*
    @Query(nativeQuery = true, value = "select * from demande_conge where id =?")
RequestForm findByidsql(Long id);
*/


}
