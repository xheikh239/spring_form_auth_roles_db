package com.example.demo.repository;



import java.util.List;

import javax.transaction.Transactional;

import com.example.demo.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository(value = "UserRepository")
public interface UserRepository extends JpaRepository<User, Integer> {


User findByMatricule(String matricule);

List<User> findAllByOrderById();

User findById(int id);


@Transactional
@Modifying
@Query(value = "update user set active=false where id=?", nativeQuery = true)
void blockUser(int id);

@Transactional
@Modifying
@Query(value = "update user set active=true where id=?", nativeQuery = true)
void unBlockUser(int id);

}
