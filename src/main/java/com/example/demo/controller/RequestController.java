package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.model.RequestForm;
import com.example.demo.service.RequestService;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.BindingResult;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/user/req")
public class RequestController {

	private final RequestService requestService;

	public RequestController(RequestService requestService, UserService userService) {
		this.requestService = requestService;

	}

	// display list of requests
	@GetMapping("/list")
	public ResponseEntity<List<RequestForm>> getAllRequests() {
		List<RequestForm> requests = requestService.findAllRequests();
		return new ResponseEntity<>(requests, HttpStatus.OK);
	}

	@GetMapping("/find/{id}")//find by id
	public ResponseEntity<RequestForm> getRequestById(@PathVariable("id") Long id){
		RequestForm FindRequestForm = requestService.findRequestById(id);
		return new ResponseEntity<>(FindRequestForm,HttpStatus.OK);
	}

	@PostMapping("/add")// create request
	public ResponseEntity<RequestForm> AddRequest(@Valid @RequestBody RequestForm requestForm, BindingResult bindingResult){
		RequestForm newRequestForm = requestService.applyRequest(requestForm);
		if (bindingResult.hasErrors()) {
			return new ResponseEntity<>(newRequestForm, HttpStatus.BAD_REQUEST);
		}
		requestService.applyRequest(requestForm);
		return new ResponseEntity<>(newRequestForm,HttpStatus.CREATED);
	}

	@PutMapping("/update/{id}") //update
	public ResponseEntity<RequestForm> updateRequest(@RequestBody RequestForm requestForm){
		RequestForm updateRequestForm = requestService.updateRequest(requestForm);
		updateRequestForm.setEmail(requestForm.getEmail());
		updateRequestForm.setRaison(requestForm.getRaison());
		updateRequestForm.setDateDebut(requestForm.getDateDebut());
		updateRequestForm.setDateFin(requestForm.getDateFin());
		return new ResponseEntity<>(updateRequestForm,HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteRequest(@PathVariable("id") Long id){
		requestService.deleteRequestById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	//new added
	@GetMapping("/manage-requests")
	public List<RequestForm> getAllActiveRequests() {
		return requestService.getAllActiveRequests();
	}


	@GetMapping(value = "/my-requests")
	public ResponseEntity<List<RequestForm>> myReqList() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String CurrentUser = authentication.getName();
		List<RequestForm> MyRequestList = requestService.getAllRequestsOfUser(CurrentUser);
	return new ResponseEntity<>(MyRequestList, HttpStatus.OK);
	}

	/*	@GetMapping(value="/manage-requests/{action}/{id}")
	public ResponseEntity<RequestForm> acceptOrRejectRequest(@PathVariable(value = "id") Long id,
															 @PathVariable(value = "action") String action ) {
		RequestForm request = requestService.fRequestById(id);
			if (action.equals("accept")) {
				request.setAccept(true);
				request.setActive(false);
			} else if (action.equals("reject")) {
				request.setAccept(false);
				request.setActive(false);
			}
		requestService.updateRequest(request);
	return new ResponseEntity<>(request, HttpStatus.OK);
	}*/
}
